package sim.shengdean;

public class Task {
	public int _id;
	public double _pay;

	public Agent _worker;
	public double _quality;

	public Task(int id) {
		_id = id;
	}

	public Task(int id, double pay) {
		_id = id;
		_pay = pay;
	}
}
