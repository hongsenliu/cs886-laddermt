package sim.shengdean;

import java.util.ArrayList;
import java.util.Date;

import cern.jet.random.Normal;
import cern.jet.random.engine.DRand;

public class Agent implements Comparable<Agent> {
	// answers should form a normal distribution
	public int _id;
	public double _mean;
	public double _variance;
	public Normal _dist;

	public ArrayList<Task> _completed = new ArrayList<Task>();

	public double _score;
	public ArrayList<Double> _pastScore = new ArrayList<Double>();

	public Agent(int id, double mean, double var) {
		_id = id;
		_mean = mean;
		_variance = var;
		_dist = new Normal(_mean, _variance, new DRand(id));
	}

	public double doTask(Task task) {
		task._worker = this;
		_completed.add(task);

		// generate quality from normal distribution
		double res = calculateQuality();
		return task._quality = res;
	}

	public double calculateQuality() {
		double quality = _dist.nextDouble();
		// System.out.println("quality=" + quality);
		return Math.max(0, Math.min(1, quality));
	}

	/**
	 * XXX: Calculate score based on previous work history. Used for ranking.
	 */
	// public double calculateScore() {
	// int n = _completed.size();
	// double score = 0;
	// double mul = 1.0;
	//
	// for (int i = n - 1; i >= n - 10 && i >= 0; i--) {
	// Task task = _completed.get(i);
	// score += task._quality * mul;
	// mul -= 0.1;
	// }
	//
	// _pastScore.add(score);
	//
	// return _score = score;
	// }

	// FSRS score calculator
	public double calculateScore() {
		int n = _completed.size();
		double score = 0;
		double mul = 1.0;

		for (int i = n - 1; i >= n - 10 && i >= 0; i--) {
			Task task = _completed.get(i);
			score += task._quality * mul;
			mul -= 0.1;
		}
		_pastScore.add(Math.floor(score));
		return _score = score;
	}

	/**
	 * XXX: Given a pay value, calculate the agent's attraction to the job.
	 */
	public double calculateAttraction(double pay, double basePay) {
		// naive linear function
		// return 0.5 * (pay / basePay);
		// y = x^2 / 2 * avg^2
		return pay * pay / (2 * basePay * basePay);
	}

	@Override
	public int compareTo(Agent o) {
		double com = _score - o._score;

		if (com < 0)
			return -1;
		else if (com == 0)
			return 0;
		else
			return 1;
	}
}
