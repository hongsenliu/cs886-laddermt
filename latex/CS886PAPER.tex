\documentclass[pdftex,a4paper,10pt]{sig-alternate}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{cite}
\usepackage{mathabx}

\usepackage{algorithm}
\usepackage{url}

\usepackage{color}
\usepackage{algpseudocode}


\usepackage[pdftex]{hyperref}
\hypersetup{citecolor=NavyBlue}

% Captions
\usepackage{caption}
\renewcommand{\captionfont}{\small\em}

% lists
\usepackage{enumitem}

% set tables and figures to use the same counter
\makeatletter
\renewcommand*{\thetable}{\arabic{table}}
\renewcommand*{\thefigure}{\arabic{figure}}
\let\c@table\c@figure
\makeatother


\usepackage{times}


\linespread{1.1}

\begin{document}
\title{LadderMT - Dynamic Ranking of Crowds to De-Incentivise Low-Quality Workers}
\author{Sheng-Dean Chang, Tzu-Yang Yu, HongSen Liu}
\maketitle

\begin{abstract}

Online labor markets is a new alternative for task givers to find interim workers and for workers to earn extra pocket money. Amazon Mechanical Turk, MicroTask and ClickWorker are just some examples of online labor markets. However, the quality of the work produced is dependent on the quality of the worker. Because tasks are usually designed to be completed multiple times, it makes sense to encourage quality workers to come back and do more tasks while discouraging low-quality workers to find other tasks that are better-suited to the workers' skill set. We propose a ranking system to do exactly that. The system would classify workers into different tiers or brackets based on their past work quality. We then adjust the payout to the workers based on the tier they are in. We show that by adjusting the payout based on the workers' previous history, we can indeed lower the number of low-quality workers, thereby increasing overall task quality and at the same time lowering the amount of capital spent.

\end{abstract}

\section{Introduction}

Computers were created to solve problems quickly. However, there are some problems that humans can solve intuitively yet are difficult for computers to solve. Problems such as image labeling and audio transcribing requires an extensive set of training samples for computers to learn from before they can start solving problems by themselves. The training samples must be provided by humans and the quality of the samples have a direct effect on the output from the computers. In empirical research, studies often require subjective opinions and feelings such as trustworthiness and attractiveness. Problems related to the quantification of subjective opinions and feelings are very difficult for computers to simulate but easy for humans to answer.

Because of these obstacles, a demand for human computation substantialized. Online labor markets such as Amazon Mechanical Turk, MicroTask and ClickWorker came into form to connect task givers to interim workers. The task requesters describe a task and post a reward for completing the task. After a worker has submitted a completed task, the requester can validate the submission, either by accepting it if it is of acceptable quality or rejecting it if it is of poor quality. Often, a posted task can be completed multiple times. Repetition is beneficial to the worker as he or she accumulates experience with the problem; however, it is beneficial to the requester only if the worker's submissions are of good quality. 

In online labor markets, we believe with less low-quality workers contributing more noise than signal, the task would converge much quicker to a stable answer. We also believe with less low-quality workers taking up less task quota, the requester could save substantial amounts of money. Thus, it is reasonable to design incentives for quality workers to encourage them to complete more tasks while discouraging low-quality workers to find other tasks that are better-suited for their skill set. We believe our contributions in this paper can raise task accuracy and save budget spending at the same time.

In this paper, we propose a method to de-incentivise low-quality workers. Our system, LadderMT, first ranks the workers by the quality of their work. Rather than paying a flat reward like traditional online markets, the system adjusts the reward as a function of the workers' rank. The higher a worker is ranked, the higher the reward, and vice versa. We evaluate our system based on its effectiveness to attract top-tier workers to return and complete more tasks while repelling bottom-tier workers, the total amount of capital spent and the overall quality of completed tasks. We compare these metrics with traditional online labor markets and show that by adjusting the payout based on the workers' previous history, we can indeed lower the number of low-quality workers, thereby increasing overall task quality and at the same time lowering the amount of capital spent.

\section{Related Work}
Traditional crowdsourcing methods try to attract workers with financial incentives to make up for completing tasks that are tedious and boring. Von Ahn et al. identified this problem and proposed a novel approach that channels human computation power through computer games, called Games with a Purpose (GWAP). In their experiments, they conclude that people play GWAPs not because they are personally interested in solving a computational problem, but because they wish to be entertained
\cite{VonAhn}.

GWAPs are successful because workers' utility is compensated with fun rather than money. However, this shrinks the pool of potential workers because workers who are driven completely by monetary incentives will not be attracted to participate at all. LadderMT tries to incorporate GWAP mechanics into traditional online labor markets. We think by leveraging both financial incentives and entertainment, we can produce a crowdsourcing environment where both work and play are involved, thus appealing to people who are either financially-oriented or entertainment-oriented. People who are looking to be entertained could have fun while making some money on the side; people who treat online labor markets as a serious occupation could enjoy the process because of GWAP's mechanics.

In order to make our system fun to use, we embed a ranking system into LadderMT. Ranking systems are widely used in modern competitive games. Most ranking systems in games are skill based, and their purpose is to identify and track the skills of gamers in a game in order to be able to match them into competitive matches. In addition, ranking systems have been proposed for many sports but possibly the most prominent ranking system used today is ELO. The first ELO rating system, created by Arpad Elo, is developed mainly for chess, and was then adopted by many chess federations, and eventually by organizations for other games. The ELO rating system has several practical uses. It can be used for pairing purpose in tournaments. Ratings are also used for tournament sectioning and prize eligibility. Ratings can also be used as a qualifying system for elite tournaments or events, and the most useful service of the rating system is that it allows players at all levels to monitor their progress as they become better players. We believe that the effect of ranking systems will increase workers' incentive to work harder and produce more quality works.

\section{Ranking System}

Our ranking system, LadderMT, has two goals. First, it assigns a score for each participating worker based on their previous work quality. Then, it adjusts payout to workers based on their score when they participate in future tasks.

\subsection{Worker Scoring Rules}

LadderMT keeps track of the workers' task history and assigns a score based on the combined quality of previous tasks. Each completed task is assigned a weight value which affects its leverage in determining the worker's score. The weight acts as a reflection to the worker's recent quality, thus more recent tasks are assigned heavier weights while historical tasks are assigned lower weights.

Our weight assignment decreases linearly. We assign a weight of 1.0 to the most recently completed task. Then we decrement its value by 0.1 and assign the weight to next completed task and repeat until the weight drops to 0.

For example, suppose a worker completes tasks with a constant quality of 80\%. After completing the first task, he is assigned a score of $80\% \times 1 = 0.8$. After completing the second task, his score will become $80\% \times 1 + 80\% \times 0.9 = 1.52$. His score will eventually converge at $80\% \times (1.0 + 0.9 + \cdots + 0.1) = 4.4$.

We think there are several benefits for adopting this rule. First, assigning heavier weights for the most recent tasks gives us an accurate profile of the workers' current capabilities. LadderMT can quickly detect workers that are submitting low-quality work and punish them by lowering their score. Second, LadderMT takes experience into consideration by summing up previous qualities rather than averaging them. This controls the speed of newcomers bubbling through the ranks and lets workers with more experience have a competitive edge over newcomers. Given two workers of equal quality, the worker with more work experience will rank higher. In order to rise to the top, a worker not only has to submit high-quality results, but also complete more tasks.

\subsection{Adjusted Payout Rules}

The requester sets a base pay ($B$), a maximum ($H$) and minimum ($L$) amount of pay a worker can be rewarded for completing a task. When a newcomer enters the system, he is rewarded the base pay. Once each worker is assigned a score, LadderMT sorts the workers in decreasing order, generating a ranking. Their pay is then adjusted by payment rules.

\subsection{Linear Payment Rule (LPR)}

The linear payout rule rewards the highest ranking worker with the highest pay set and decreases linearly with regard to the worker's rank, with the lowest ranking worker rewarded with the lowest pay set.

Let $i_w$ denote a worker's rank, $w$ denote the total number of workers in the ranking system; then the worker's pay ($P$) could be formulated as:
\[ P = L + (H-L)\cdot((w-i_w)/w) \]

\subsection{Bracket Payment Rule (BPR)}

The bracket payout rule splits the ladder into partially ordered groups by flooring workers' scores and grouping workers with the same floored score in one bracket. Similar to the linear payout rule, the bracket payout rule rewards the highest ranking bracket with the highest pay set and decreases linearly with regard to the number of brackets, with the lowest ranking bracket rewarded with the lowest pay set. Each member in the group receives the same pay.

For example, if a worker has a raw score of 0.58, after flooring his score, he will be assigned to the bracket of score 0, the lowest bracket in the system. In contrast, if a worker has the highest possible score of 5.5, he will be assigned to the highest bracket of score 5 after flooring his raw score.

Workers are paid based on their bracket when completing tasks. The higher the bracket a worker is in, the more money the worker will be awarded for completing tasks. Let $b$ denote the total number of brackets, $i_b$ denote the rank of the bracket; the pay difference $\Delta_P$ between brackets and worker pay can be formulated as
\[ \Delta_P = (H - L) / b \]
\[ P = L + (b-i_b) \cdot \Delta_P \]

For example, suppose $(L,H,b)=(20,60,6)$, the interval payment will be 8 after applying the formula above. Therefore, workers will be paid 20, 28, 36, 44, 52, and 60 depending on the bracket he is in. 


\section{Simulation}

We simulate our model by generating $w$ workers to complete $t$ tasks. The same set of workers is used in a fixed pay environment and our LadderMT environment.

\subsection{Tasks}

A task is a unit of work that is assigned to workers for completion. We assume that tasks are identical; their base pay is the same and they take the same amount of time to complete.

\subsection{Workers}

Worker complete tasks. Their goal is to maximize their utility, which is the amount of money tasks pay.

Each worker is initialized with a randomly generated expected quality $e(Q) \in \{x \in \mathbb{R} \mid 0 \leq x \leq 1\}$ and standard deviation $\sigma \in \{x \in \mathbb{R} \mid 0 \leq x\leq 1\}$. We assume that workers' output quality should conform to a normal distribution. Thus, we create a Gaussian random number generator for each worker with his expected quality as mean and standard deviation as is to generate work qualities $Q \in \{x \in \mathbb{R} \mid 0 \leq x\leq 1\}$.

Given a task, workers decide whether or not to participate in completing it. We call the probability that a worker will participate in completing a task the \textit{attractiveness} of the task. Let $\widebar{P}$ denote the entire market's average task pay; we model a task's attractiveness ($A$) as:
\[ A = P^2 / (2 \cdot \widebar{P}^2) \]

When $A \geq 1$, workers will repeatedly work on the tasks; when $A = 0$, workers will find the task unattractive and find other activities to do. When a task pays the same amount as the market average $(P = \widebar{P})$, there is equal chance that a worker will participate in completing a task or other activities. Since workers aim to maximize their utility, we assume that workers who choose to opt out of our tasks found some other task in the market that pays higher for the same amount of work; whatever other tasks workers engage in, it will always take the same amount of time as completing our task; thus all workers start on some task at the same time, work in parallel and complete their tasks at the same time. Then again, workers come back to decide on what tasks to engage in.

If a worker finds the task unattractive, he opts out from participating in completing a task. The task is assigned to the next worker, who will again evaluate whether or not the task is worth his time. This loop repeats until all tasks are completed.

\subsection{Fixed Pay Environment}

The fixed pay environment is modeled after conventional online labor market Amazon Mechanical Turk. For simplicity, we let $B = \widebar{P}$; every round, each worker is equally willing and unwilling to participate in the task. When a worker completes a task, the resulting quality is randomly sampled from the worker's quality distribution. We assume that the worker is rewarded with a fixed payment despite work quality.

\subsection{Dynamic Pay (LadderMT) Environment}

Newcomers start off being paid the base amount. After completing a task, each worker's score is recalculated based on the proposed scoring rules. We then adjust their payment for future tasks based on their score using the proposed payment rules.


\section{Performance Evaluation}

We evaluate our model based on its effectiveness to attract top-tier workers to return and complete more tasks while repelling bottom-tier workers, the total amount of capital spent and the overall quality of completed tasks. For simplicity, we run a small-world simulation using 5 workers completing a total of 100 tasks; values regarding payment are set to $(\widebar{P}, B, L, H)=(50, 50, 20, 60)$. We set their expected quality by hand. 

\subsection{Fixed Pay Environment}

%The Performance Based Rating System (PBRS) is to pay workers base on their score. A worker who has the highest score will receive the maximum pay, and the lowest score will receive the lowest pay. We sort workers base on their score begin from the highest score, so for each worker i, we can have following payout rule:
%Payment = (Maximum Pay – Minimum Pay) * ((No. Workers – i)  /  No. Workers)


We show results of these five workers working in a fixed pay environment. The pay is fixed to the base pay. Figure \ref{fig:FIG1} shows workers' $E(Q)$ and $\widebar{Q}$. We manually set $E(Q)$ so that our workers are of diverse quality. Figure \ref{fig:FIG2} shows the task distribution among workers. Because the pay is fixed and is the same as the market average, there are no incentives for high-quality workers to do more tasks, they may do other requesters' tasks. In contrast, low-quality workers are equally incentivised as high-quality workers, taking up similar proportions of the task quota. After running 10 simulations, we obtain a mean average quality of 0.5094\% with deviation of 0.0181 spending a mean budget of \$5,000 with a deviation of 0.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{FIG1}
\caption{Workers expected work quality $E(Q)$ and average work quality $\widebar{Q}$ of completed tasks. $E(Q)$ is manually set for this experiment.}
\label{fig:FIG1}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{FIG2}
\caption{Task distribution among workers in a fixed pay environment. Bar graph represents amount paid for completing tasks, corresponding to the axis on the left; line graph represents the number of tasks completed, corresponding to the axis on the right.}
\label{fig:FIG2}
\end{figure}

\subsection{LadderMT using Linear Payout Rule}

We show results of these five same workers working in LadderMT, a dynamic pay environment using the Linear Payout Rule. Recall that after completing tasks, LadderMT first calculate each worker's score, then adjust their pay for future tasks based on their score. Figure \ref{fig:FIG4} shows each worker's score after completing a task. LadderMT assigns a higher score to Worker 0 because of his high-quality submissions. This results in a higher rank for Worker 0, netting him higher pay for future tasks. Figure \ref{fig:FIG3} shows the task distribution among workers. Worker 0 completes much more tasks than all other workers because he gets paid more, making the task much more attractive to him, which results in a higher probability of him working on our tasks rather than other requesters' tasks every round. In contrast, Worker 4 has the lowest score, netting him a pay much lower than the market average; at this point, it makes sense for him to find other tasks on the market to do which pays more than our tasks. This is also why Worker 0's line is longer than Worker 4's line in Figure \ref{fig:FIG4}. After running 10 simulations, we obtain a mean average quality of 64.06\% with deviation of 2.5\% spending a mean budget of \$4715.20 with a deviation of \$106.36


\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{FIG4}
\caption{LadderMT's worker scoring rule correctly catalogs high-quality workers with higher scores than low-quality workers.}
\label{fig:FIG4}
\end{figure}


\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{FIG3}
\caption{Task distribution among workers in LadderMT, under the Linear Payout Rule.}
\label{fig:FIG3}
\end{figure}



\subsection{LadderMT using Bracket Payout Rule}

%Our second approach, is to make our rating system floored, unlike previous approach; this allows our system to eliminate many low quality workers. In FSRS, we divided our system to 6 floors (rating 0 to rating 5), and the worker's score is floored whenever needed.

We show results of these five same workers working in LadderMT, a dynamic pay environment using the Bracket Payout Rule. Figure \ref{fig:BS}, all workers start from the lowest bracket. We can see Worker 0 outperforms all other workers, since Worker 0 is a highest-quality worker; whereas Worker 4, the lowest-quality Worker, stays in the lowest bracket. Figure 7 also shows that high-quality workers are more likely to complete tasks rather than low-quality workers.  This is because low quality workers, who stay in the rank 0, will receive the lowest pay, which makes the task less attractive to them. After running 10 simulations, we obtain a mean average quality of 66.96\% with deviation of 2.08\% spending a mean budget of \$4067.20 with a deviation of \$103.3793.

We believe Bracket Payment Rule is better than Linear Payment Rule because BPR only provide higher payment when the worker is actually high-quality worker. Each worker in LPR cannot easily climb to the next level if it only produces some quality works; instead, workers need to output certain amounts of high-quality works in order to get to the next level.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{BracketScore}
\caption{Bracket Payout Rule floors the score of each worker and group them into brackets of the same score.}
\label{fig:BS}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{BracketTasks}
\caption{Task distribution among workers, under the Bracket Payout Rule}
\label{fig:BT}
\end{figure}

\subsection{Introducing Malicious Workers}

In this section, we focus on malicious workers. We assume that requesters always accept submissions from workers. We define a malicious worker as a worker that will produce low quality results when he/she performs numerous repeated tasks in a bracket. We employ the same workers as above, except they are now malicious. Figure \ref{fig:M1} presents the average qualities and expected average qualities of malicious workers. Since the workers are malicious, most of them have lower average qualities compared to expected average qualities. 

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{M1}
\caption{Task distribution among workers, under the Bracket Payout Rule}
\label{fig:M1}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{M2}
\caption{Task distribution among workers, under the Bracket Payout Rule}
\label{fig:M2}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{M3}
\caption{Task distribution among workers, under the Bracket Payout Rule}
\label{fig:M3}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{M4}
\caption{Task distribution among workers, under the Bracket Payout Rule}
\label{fig:M4}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{M5}
\caption{Task distribution among workers, under the Bracket Payout Rule}
\label{fig:M5}
\end{figure}

Since we construct the scoring system with the historical quality of works, the lower quality works will result to the lower payments. Therefore we assumed that the workers will pay more effort to contribute higher quality works, when his/her payments reached to a certain low level. First we performed the simulation with 100 tasks and 5 workers.



Figure \ref{fig:M2} shows the qualities of tasks with fixed payments. After the malicious workers completed some tasks, they starts to produce low quality works in fixed payment environment. Figure \ref{fig:M3} represents the qualities of the workers with their completed tasks. It shows that the qualities of malicious workers are not stable. 



Figure \ref{fig:M4} shows the scores with the tasks. The qualities are positively correlated to the scores. Figure \ref{fig:M5} illustrates the payments of each task. The payments are based quality. For example, the quality of Worker 0 dropped to zero at its 12th task. Its payments reduced from 60 to 44. We assumed that every worker expects a higher payment. Therefore Worker 0 contributed higher quality work after its 18th task. LadderMT drives the malicious workers to contribute more higher quality works. Then we performed 10 repetitions that simulated with 10,000 tasks and 50 malicious workers.  In the fixed payment environment, the average quality is only 2.3\%. With LadderMT, the average quality is 55\%. The average total payment is reduced 4.57\% with LadderMT payment scheme.
 
\section{Conclusion}








\bibliographystyle{abbrv}
\bibliography{thebib}
\end{document}
